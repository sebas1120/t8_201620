package estructuras;


public class TablaHash<K ,V> {

	/**
	 * Estructura que soporta la tabla.
	 */
	public NodoHash<K, V>[] nodo;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. TamaÃ±o del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		
		nodo = new NodoHash[50];
		capacidad = 50;
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		nodo = new NodoHash[capacidad];
		this.capacidad = capacidad;
	}
	
	public int darCapacidad()
	{
		return capacidad;
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
//		if(count >= capacidad/2)rehash(2*capacidad);
		int i;
		for (i = hash(llave); nodo[i] !=null; i=(i+1)%capacidad) 
		{
			if(nodo[i].getLlave().equals(llave))
			{
				nodo[i].setValor(valor);
			}
		}
		nodo[i] = new NodoHash<K,V>(llave, valor);
		count++;
	}
	public int size()
	{
		return count;
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		for(int i = hash(llave);nodo[i]!=null;i=(i+1)%capacidad)
			if(nodo[i].getLlave().equals(llave))
				return nodo[i].getValor();
		return null;
	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V eliminado = null;
		int i = hash(llave);
		while (!llave.equals(nodo[i].getLlave()))
			i = (i + 1) % capacidad;
		eliminado = nodo[i].getValor();
		nodo[i] = null;

		i = (i + 1) % capacidad;
		while (nodo[i] != null)
		{
			K keyToRedo = nodo[i].getLlave();
			V valToRedo = nodo[i].getValor();
			nodo[i] = null;
			count--;
			put(keyToRedo, valToRedo);
			i = (i + 1) % capacidad;
		}
		count--;
		if (count > 0 && count == capacidad/8) rehash(capacidad/2);
		return eliminado;
		
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una funciÃ³n de Hash, recuerde tener en cuenta la complejidad de Ã©sta asÃ­ como las colisiones.
		
		return (llave.hashCode() &  0x7fffffff)% capacidad;

		
	}
	
		//TODO: Permita que la tabla sea dinamica
	private void rehash(int j)
	{
		TablaHash<K, V> nuevaTabla;
		nuevaTabla = new TablaHash<K,V>(j);
		for (int i = 0; i < capacidad; i++)
		{
			if (nodo[i] != null)
			{
				nuevaTabla.put(nodo[i].getLlave(), nodo[i].getValor());
			}
		nodo = nuevaTabla.nodo;
		capacidad = nuevaTabla.capacidad;
		}
	}

}