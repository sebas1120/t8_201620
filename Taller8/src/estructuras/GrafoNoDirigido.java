package estructuras;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Clase que representa un grafo con peso no dirigido.
 * @author SamuelSalazar
 * @param <T> El tipo que se va a utilizar como nodos del grafo
 */
public class GrafoNoDirigido<T extends NodoInterface> implements Grafo<T> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	//Tabla y El T hace referencia a los nodos
	private TablaHash<Integer,T> nodos;

	/**
	 * Lista de adyacencia 
	 */
	private HashMap<Integer, ListaSencilla<Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 
		
	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		nodos = new TablaHash<Integer,T>(500);
		adj = new HashMap<Integer,ListaSencilla<Arco>>();
		
	}
	
	public HashMap<Integer, ListaSencilla<Arco>> darAdj()
	{
		return adj;
	}

	
	public boolean agregarNodo(T nodo) {
		//TODO implementar
		
		nodos.put(nodo.darId(), nodo);
		ListaSencilla<Arco> nueva = new ListaSencilla<Arco>();
		adj.put(nodo.darId(), nueva);
		return true;
		
	}

	@Override
	public boolean eliminarNodo(int id) 
	{
		//TODO implementar
		nodos.delete(id);
		return true;
	}

	@Override
	public ArrayList<Arco> darArcos() {
		//TODO implementar
		ArrayList<Arco> lista = new ArrayList<Arco>();
		for (int j = 0; j < nodos.size(); j++) 
		{
			ListaSencilla<Arco> listaArcos =adj.get(j);
			for (int i = 0; i < listaArcos.size(); i++) 
			{
				lista.add(listaArcos.get(i));
			}
		}
		return lista;
		
	}

	private <E extends Comparable<E>> Arco crearArco( final int inicio, final int fin, final double costo, E e, String nom )
	{
		return new Arco(buscarNodo(inicio), buscarNodo(fin), costo, e,nom );
	}

	@Override
	public ArrayList<T> darNodos() {
		//TODO implementar
		ArrayList<T> lista = new ArrayList<T>();
		for (int j = 0; j < nodos.size(); j++) 
		{
			lista.add(nodos.get(j));
		}
		return lista;
	}

	@Override
	public <E extends Comparable<E>> boolean agregarArco(int i, int f, double costo, E obj, String nom) 
	{
		//TODO implementar
		NodoInterface nuevoIni = buscarNodo(i);
		NodoInterface nuevoFin = buscarNodo(f);
		Arco nuevo = new Arco(nuevoIni, nuevoFin, costo, nom);
		adj.get(i).add(nuevo);
		adj.get(f).add(nuevo);
		return true;
	}

	@Override
	public boolean agregarArco(int i, int f, double costo, String nom) {
		return agregarArco(i, f, costo, null,nom);
	}

	@Override
	public Arco eliminarArco(int inicio, int fin) {
		//TODO implementar
		ListaSencilla<Arco> arcosIni =adj.get(inicio);
		Arco eliminado = null;
		for (int i = 0; i < arcosIni.size(); i++) 
		{
			Arco encontrado =arcosIni.get(i);
			if(encontrado.darNodoFin().darId()==fin)
			{
				eliminado=encontrado;
				encontrado=null;
				
			}
		}
		return eliminado;
		
	}

	@Override
	public NodoInterface buscarNodo(int id) {
		//TODO implementar
		return nodos.get(id);
	}

	@Override
	public ListaSencilla<Arco> darArcosOrigen(int id) {
		//TODO implementar
		return adj.get(id);
		
	}

	@Override
	public ListaSencilla<Arco> darArcosDestino(int id) {
		//TODO implementar
		return adj.get(id);
	}

}
