package estructuras;

import java.util.Collection;



/**
 * Clase que representa un lista sencillamente encadenada. La lista no acepta elementos nulos
 * @param <T> tipo de elementos almacenados en la lista
 */
public class ListaSencilla<T>  {

	
	/**
	 * Primer elemento de la lista
	 */
	private Nodo<T> primero;
	
	/**
	 * Crea una lista sencillamente encadenada
	 * post:	El primer elemento se ha inicializado en null
	 */
	public ListaSencilla()
	{
		primero = null;
	}
	
	/**
	 * Agrega el elemento por par�metro al final de la lista
	 * @param t el elemento que se desea agregar.
	 * @return true en caso de que la lista se haya modificado (se agreg� el elemento) o false en caso contrario
	 * @throws NullPointerException Se lanza en caso que T sea nulo
	 * @throws IllegalArgumentException Se lanza en caso que ya exista un elemento con el identificador de t en la lista
	 */
	public boolean add(T t) throws NullPointerException, IllegalArgumentException
	{
		
		if( t == null)
		{
			throw new NullPointerException("La lista no acepta valores nulos");
		}
		
		boolean agregado = false;
		if( primero == null)
		{
			primero = new Nodo<T>(t);
			agregado = true;
		}
		else
		{
			
				Nodo<T> actual = primero;
				while(actual.darSiguiente() != null)
				{
					actual = actual.darSiguiente();
				}
				actual.cambiarSiguiente(new Nodo<T>(t));
				agregado = true;
			
		}
		return agregado;
	}

	/**
	 * Agrega el elemento t en la posici�n pos de la lista
	 * post:  Los elementos posteriores a pos se han movido una posici�n a la derecha
	 * @param pos la posici�n donde se desea agregar elemento
	 * @param t el elemento que se desea agregar
	 * @throws NullPointerException En caso que t sea nulo
	 * @throws IndexOutOfBoundsException si la posici�n est� fuera de la lista (pos < 0 || pos > size())
	 * @throws IllegalArgumentException si ya existe el elemento t en la lista
	 */
	public void add(int pos, T t) throws NullPointerException, IndexOutOfBoundsException, IllegalArgumentException
	{
		if( t == null)
		{
			throw new NullPointerException("La lista no acepta valores nulos");
		}
		
		if(pos < 0 || pos > size())
		{
			throw new IndexOutOfBoundsException();
		}
		Nodo<T> nuevo = new Nodo<T>(t);
		
		if( pos == 0 )
		{
			nuevo.cambiarSiguiente(primero);
			primero = nuevo;
		}
		else
		{
			int i = 0;
			Nodo<T> actual = primero;
			while(actual != null && i < pos-1)
			{
				actual = actual.darSiguiente();
				i++;
			}
			nuevo.cambiarSiguiente(actual.darSiguiente());
			actual.cambiarSiguiente(nuevo);
		}
		
	}


	/**
	 * Agrega los elementos de la colecci�n c al final de la lista en el orden que los devuelva el iterador de c
	 * @param la colecci�n de elementos que se desea agregar
	 * @return true en caso de que se agregue al menos un elemento a la lista o false en caso contrario.
	 * @throws NullPointerException Se lanza en caso que alg�n t en c sea nulo. En este caso los elementos previos a t se agregan y se detiene la operaci�n
	 * al encontrar un elemento nulo.
	 * @throws IllegalArgumentException Se lanza en caso que ya exista un elemento t de c en la lista.En este caso los elementos previos a t se agregan y se detiene la operaci�n
	 * al encontrar un elemento repetido.
	 */


	/**
	 * Agrega los elementos de la colecci�n c de la lista en el orden que los devuelva el iterador de c iniciando en la posici�n que llega por par�metro 
	 * post:  Los elementos posteriores a pos se han movido pos posiciones a la derecha
	 * @param la colecci�n de elementos que se desea agregar
	 * @param la posici�n a partir de la cual se quieren agregar los elementos en la lista
	 * @return true en caso de que se agregue al menos un elemento a la lista o false en caso contrario.
	 * @throws NullPointerException Se lanza en caso que alg�n t en c sea nulo. En este caso los elementos previos a t se agregan y se detiene la operaci�n
	 * al encontrar un elemento nulo.
	 * @throws IllegalArgumentException Se lanza en caso que ya exista un elemento t de c en la lista.En este caso los elementos previos a t se agregan y se detiene la operaci�n
	 * al encontrar un elemento repetido.
	 * @throws IndexOutOfBoundsException si la posici�n est� fuera de la lista (pos < 0 || pos > size())
	 */
	

	/**
	 * Eliminad todos los elementos de la lista
	 * post: se han borrado todos los elementos de la lista
	 */
	public void clear() 
	{
		primero = null;
	}

	/**
     * Indica si la lista contiene el objeto indicado
     * @param objeto el objeto que se desea buscar en la lista. objeto != null
     * @return true en caso que el objeto est� en la lista o false en caso contrario
     */
    public boolean contains( Object objeto )
    {
        boolean contiene = false;
           
            Nodo<T> nodo = primero;
            while( nodo!= null && !contiene)
            {
                if(nodo.darElemento( ).equals( objeto ))
                {
                    contiene = true;
                }

                nodo = nodo.darSiguiente( );
            }
        
        return contiene;
    }

    /**
     * Indica la posici�n del objeto que llega por par�metro en la lista
     * @param objeto el objeto que se desea buscar en la lista. objeto != null
     * @return la posici�n del objeto o -1 en caso que no se encuentre en la lista
     */
    public int indexOf( Object objeto )
    {
       
        int pos = -1;
        Nodo<T> nodo = primero;
        if(contains( objeto ))
        {
            pos=0;
            if(primero.darElemento( ).equals( objeto ))
            {
                return pos;
            }
            else
            {

                while( nodo.darSiguiente( )!=null )
                {

                    pos++;
                    if(nodo.darSiguiente( ).darElemento( ).equals( objeto ))
                    {
                        return pos;
                    }
                    nodo = nodo.darSiguiente( );
                }
            }
        }
        return pos;
    }


	/**
	 * Devuelve el elemento en la posici�n dada por par�metro
	 * @param pos la posici�n de la que se desea conocer el elemento
	 * @throws IndexOutOfBoundsException si la posici�n est� fuera de la lista (pos < 0 || pos > size())
	 */
	public T get(int pos) 
	{
		if(pos < 0 || pos > size())
		{
			System.out.println(""+pos);
			throw new IndexOutOfBoundsException();
		}
		int i = 0;
		Nodo<T> actual = primero;
		while(actual != null && i < pos)
		{
			actual = actual.darSiguiente();
			i++;
		}
		return actual.darElemento();
	}

	/**
     * Devuelve el nodo de la posici�n dada
     * @param pos la posici�n  buscada
     * @return el nodo en la posici�n dada 
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public Nodo<T> getNodo( int pos )
    {
        
        Nodo<T> nodo = primero;
        Nodo<T> encontrado = null;
        int contador = 0;
        if(pos<0 || pos >=size( ))
        {
            throw new IndexOutOfBoundsException( );
        }
        else
        {
            if(pos==0)
            {
                return primero;
            }
            else
            {
                while(nodo.darSiguiente( )!= null && encontrado==null)
                {
                    contador++;
                    if(contador==pos)
                    {
                        encontrado=nodo.darSiguiente( );
                    }
                    else
                    {
                    nodo = nodo.darSiguiente( );
                    }
                }
            }
        }
        return encontrado;
    }
	
	/**
	 * Indica si la lista esta vac�a
	 * @return true en caso que la lista no tenga elementos o false en caso contrario
	 */
	public boolean isEmpty() 
	{
		return primero == null;
	}
	

	/**
     * Elimina el nodo que contiene al objeto que llega por par�metro
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
    public boolean remove( Object objeto )
    {
        
        
        if(contains( objeto ))
        {
            
            if(primero.darElemento( ).equals( objeto ))
            {
                
                primero = primero.darSiguiente( );
                return true;
            }
            else
            {
                
                for( int i = 1; i < size( ); i++ )
                {
                    if(getNodo( i ).darElemento( ).equals( objeto ))
                    {
                        
                        Nodo<T> anterior = getNodo( i-1 );
                        Nodo<T> actual = anterior.darSiguiente( );           
                        Nodo<T> siguiente = actual.darSiguiente( );
                        
                        anterior.cambiarSiguiente( siguiente ); 
                        return true;
                    }
                }
                                  
                              

                
            }
        }
        return false;


    }

    /**
     * Elimina el nodo en la posici�n por par�metro
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public T remove( int pos )
    {
       
        Nodo<T> eliminado = null;
        if(pos<0 || pos>=size( ))
        {
            throw new IndexOutOfBoundsException( );
        }
        else
        {
            if(pos==0)
            {
                eliminado = primero;
                primero = primero.darSiguiente( );

            }
            else
            {
                Nodo<T> anterior = getNodo( pos-1 );
                Nodo<T> actual = anterior.darSiguiente( );           
                Nodo<T> siguiente = actual.darSiguiente( );

                anterior.cambiarSiguiente( siguiente );
                eliminado = actual;
            }
            return eliminado.darElemento( );
        }
    }


	/**
	 * Reemplaza el elemento de la posici�n pos por el elemento t
	 * @param pos la posici�n que se desea reemplazar
	 * @param t el elemento con el que se reeemplazar� el elemento de pos
	 * @return el elemento que se encontraba previamente en pos
	 * @throws NullPointerException Se lanza en caso que  t  sea nulo. 
	 * @throws IllegalArgumentException Se lanza en caso que ya exista el elemento t en la lista.
	 * @throws IndexOutOfBoundsException si la posici�n est� fuera de la lista (pos < 0 || pos > size())
	 */
	public T set(int pos, T t) 
	{
		if( t == null)
		{
			throw new NullPointerException("La lista no acepta valores nulos");
		}
		
		if(pos < 0 || pos > size())
		{
			throw new IndexOutOfBoundsException();
		}
		T eliminado = null;
		Nodo<T> nuevo = new Nodo<T>(t);
		if( pos == 0 )
		{
			eliminado = primero.darElemento();
			nuevo.cambiarSiguiente(primero.darSiguiente());
			primero = nuevo;
		}
		else
		{
			Nodo<T> actual = primero;
			int  i = 0;
			while(actual != null && i < pos-1)
			{
				actual = actual.darSiguiente();
				i++;
			}
			eliminado = actual.darSiguiente().darElemento();
			nuevo.cambiarSiguiente(actual.darSiguiente().darSiguiente());
			actual.cambiarSiguiente(nuevo);
		}
		return eliminado;
	}

	/**
	 * Indica la cantidad de elementos en la lista
	 * @return n�mero de elementos en la lista
	 */
	public int size() 
	{
		Nodo<T> actual = primero;
		int  i = 0;
		while(actual != null)
		{
			actual = actual.darSiguiente();
			i++;
		}
		return i;
	}


	/**
	 * Devuelve una sublista de nuevos elementos, que inicia en el elemento de la posici�n desde (inclusivo) y termina en el elemento de la posici�n hasta (exclusivo)
	 * @param desde Posici�n desde la que se quiere construir la sublista (inclusivo)
	 * @param hasta Posici�n hsata la que se quiere construir la sublista (exclusivo)
	 * @return Una ListaSencilla con los elementos de la posici�n desde hasta la posici�n hasta. En caso que desde >= hasta  se devuelve una lista vacia
	 * @throws IndexOutOfBoundsException si las posiciones desde y hasta est�n fuera de la lista (pos < 0 || pos > size())
	 */
	public ListaSencilla<T> subList(int desde, int hasta) 
	{
		if(desde < 0 || desde > size() || hasta < 0 || hasta > size())
		{
			throw new IndexOutOfBoundsException();
		}
		
		ListaSencilla<T> resp  = new ListaSencilla<T>();
		Nodo<T> actual = primero;
		int  i = 0;
		while(actual != null && i < desde) 
		{
			actual = actual.darSiguiente();
			i++;
		}
		
		while(actual != null && i < hasta)
		{
			resp.add(actual.darElemento());
			actual = actual.darSiguiente();
			i++;
		}
		return resp;
	}

	/**
	 * Devuelve un nuevo arreglo de objetos con los elementos T de la lista
	 * @return arreglo con los elementos de la lista
	 */
	public Object[] toArray() {
		
		Object[] os = new Object[size()];
		Nodo<T> actual = primero;
		int  i = 0;
		while(actual != null)
		{
			os[i] = actual.darElemento();
			actual = actual.darSiguiente();
			i++;
		}
		return os;
	}

	/**
	 * Retorna un arreglo de T con los elementos T de la lista. Si los elementos de la lista caben en el par�metro se devuelve all�
	 *  de lo contrario se devuelven en un nuevo arreglo. En caso que el par�metro tenga m�s espacio que elementos en la lista el elemento
	 *  inmediatamente posterior al �ltimo elemento de la lista en el arreglo debe ser null
	 *  @param array arreglo donde se guardaran los elementos de la lista
	 *  @return un arreglo con los elementos T de la lista
	 *  @throws NullPointerException Si el arreglo por par�metro es nulo
	 */
	public <T> T[] toArray(T[] array) throws NullPointerException
	{
		if( array == null )
		{
			throw new NullPointerException("El arreglo no puede ser nulo");
		}
		if( array.length < size())
		{
			return (T[])toArray();
		}
		else
		{
			Nodo actual = primero;
			int  i = 0;
			while(actual != null)
			{
				array[i] = (T)actual.darElemento();
				actual = actual.darSiguiente();
				i++;
			}
			if(array.length > size())
			{
				array[i] = null;
			}
			return array;
		}
		
		
	}

	public T first() {
		return primero.darElemento();
	}

}
