package estructuras;

import java.io.Serializable;

import javax.naming.NoPermissionException;

/**
 * Nodo de una lista sencillamente encadenada
 */
public class Nodo <T> {
	
	/**
	 * Elemento almacenado por el nodo
	 */
	private T elem;
	
	/**
	 * El nodo siguiente
	 */

	private Nodo<T> siguiente;
	
	
	
	public Nodo(T nElem)
	{
		elem = nElem;
		siguiente=null;

	}
	
	/**
	 * Devuelve el nodo siguiente
	 * @return siguiente
	 */
	public Nodo<T> darSiguiente()
	{
		
		return siguiente;

	}
	
	/**
	 * Cambia el nodo siguiente por el nodo que llega por par�metro
	 * post: 	Se ha cambiado siguiente por el nodo que llega por par�metro
	 * @param nSiguiente el nuevo siguiente.
	 */
	public void cambiarSiguiente(Nodo<T> nSiguiente)
	{
		siguiente = nSiguiente;

	}
	
	/**
	 * Deuvleve el elemento almacenado por el nodo
	 * @return elem
	 */
	public T darElemento()
	{
		return elem;
	}


}
