package estructuras;

public class NodoGrafo implements NodoInterface 
{
	private int id;

	public NodoGrafo(int id)
	{
		this.id=id;
	}
	
	@Override
	public int compareTo(NodoInterface o) 
	{
		if(o.darId()>id)
			return 1;
		else if(o.darId()<id)
			return -1;
		else return 0;
	}

	@Override
	public int darId() {
		// TODO Auto-generated method stub
		return id;
	}

}
