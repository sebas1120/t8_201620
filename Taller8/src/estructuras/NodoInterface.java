package estructuras;

/**
 * Interface que representa un nodo de un grafo.
 * @author SamuelSalazar
 *
 */
public interface NodoInterface extends Comparable<NodoInterface> {
	
	/**
	 * Identificador único del nodo
	 * @return id
	 */
	public int darId();
	

}
