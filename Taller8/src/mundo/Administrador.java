package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.ListaSencilla;
import estructuras.Nodo;
import estructuras.NodoGrafo;
import estructuras.NodoInterface;

/**
 * Clase que representa el administrador del sistema integrado de transporte
 * @author SamuelSalazar
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	/**
	 * Grafo que modela el sistema integrado de transporte
	 */
	//TODO Declare el grafo que va a modela el sistema
	private Grafo<Estacion> grafo;

	/**
	 * Construye un nuevo administrador del SITP
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo = new GrafoNoDirigido<Estacion>();
	}

	/**
	 * Devuelve todas las rutas que pasan por un paadero con nombre dado
	 * @param nombre 
	 * @return Arreglo con los nombres de las rutas que pasan por el estacion requerido
	 */
	public String[] darRutasParadero(String nombre)
	{
		//TODO Implementar
		int id = 0;
		Estacion temp =buscarParadero(nombre);
		if(temp!=null)
		{
			id= temp.darId();
		}
		else
		{
			return null;
		}
		
		HashMap<Integer, ListaSencilla<Arco>> rutasO =grafo.darAdj();
		ListaSencilla<Arco> arcos = rutasO.get(id);
		String[] rutas = new String[arcos.size()];
		for (int i = 0; i < arcos.size(); i++) 
		{
			rutas[i] = arcos.get(i).darNombre() + " con costo " + arcos.get(i).darCosto();
		}
		
		return rutas;
	}

	/**
	 * Devuelve la distancia que hay entre los paraderos mas cercanos del sistema.
	 * @return distancia minima entre 2 paraderos
	 */
	public double distanciaMinimaParaderos()
	{
		//TODO Implementar
		double minimo = Double.POSITIVE_INFINITY;
		ArrayList<Arco> arcos = grafo.darArcos();
		for (int i = 0; i < arcos.size(); i++) 
		{
			if(arcos.get(i).darCosto()<minimo)
			{
				minimo=arcos.get(i).darCosto();
			}
		}
		return minimo;
	}


	public Estacion darParadero(int id)
	{
		//TODO Implementar
		Estacion buscado =(Estacion)grafo.buscarNodo(id);
		return buscado;
	}
	/**
	 * Devuelve la distancia que hay entre los paraderos más lejanos del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaParaderos()
	{
		//TODO Implementar
		double maximo = 0;
		ArrayList<Arco> arcos = grafo.darArcos();
		for (int i = 0; i < arcos.size(); i++) 
		{
			if(arcos.get(i).darCosto()>maximo)
			{
				maximo=arcos.get(i).darCosto();
			}
		}
		return maximo;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		int id = 0;
		String estacion;
		FileReader f = new FileReader(RUTA_PARADEROS);
		BufferedReader b = new BufferedReader(f);
		estacion = b.readLine();
		int sizeEstacion = Integer.parseInt(estacion);

		while((estacion=b.readLine())!=null) 
		{
			String[] sergito = estacion.split(";");

			Estacion nueva = new Estacion(Double.parseDouble(sergito[1]),Double.parseDouble(sergito[2]),sergito[0], id);
			grafo.agregarNodo(nueva);
			id++;
		}
		b.close();
		System.out.println("Se han cargado correctamente "+grafo.darNodos().size()+" Estaciones");

		//		TODO Implementar
		String rutas;
		FileReader r = new FileReader(RUTA_RUTAS);
		BufferedReader g = new BufferedReader(r);
		rutas=g.readLine();
		int numeroRutas = Integer.parseInt(rutas);
		rutas=g.readLine();
		while((rutas=g.readLine())!=null) 
		{
			
			String nombreRuta = rutas;
			rutas= g.readLine();
			
			int cantidadEstaciones = Integer.parseInt(rutas);
			rutas = g.readLine();
			
			String[] objetos1 = rutas.split(" ");
			String estacionInicio = objetos1[0];
			if(buscarParadero(estacionInicio)!=null)
			{
				
				Estacion estacionIni = buscarParadero(estacionInicio);
				while((rutas=g.readLine())!=null && !rutas.contains("-"))
				{
					String[] objetos = rutas.split(" ");
					String estacionFin = objetos[0];
					double peso = Double.parseDouble(objetos[1]);
					
					if(buscarParadero(estacionFin)!=null)
					{
						Estacion estaFin = buscarParadero(estacionFin);
						grafo.agregarArco(estacionIni.darId(), estaFin.darId(), peso, nombreRuta);
					}
				}
			}
			else
			{
				while(!rutas.contains("-"))
				{
					rutas=g.readLine();
				}
			}


		}
		g.close();
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().size()+" arcos");
	}

	/**
	 * Busca el estacion con nombre dado<br>
	 * @param nombre el nombre del estacion buscado
	 * @return estacion cuyo nombre coincide con el parametro, null de lo contrario
	 */
	public Estacion buscarParadero(String nombre)
	{
		//TODO Implementar
		Estacion buscado = null;
		ArrayList<Estacion> paraderos =grafo.darNodos();
		for (int i = 0; i < paraderos.size(); i++) 
		{
			Estacion temp =paraderos.get(i);
			if(temp.darNombre().equals(nombre))
			{
				buscado= temp;
			}
		}
		return buscado;
	}

}
